import selenium


def hello_world():
    return f"hello_world from testcode in demo_layer. It also includes selenium version {selenium.__version__} for no reason!"
