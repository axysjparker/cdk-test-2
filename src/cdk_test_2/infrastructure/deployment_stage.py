import aws_cdk.core as core
from cdk_test_2.cdk_test_2 import CDKTest2


class CDKTest2Stage(core.Stage):
    def __init__(self, scope: core.Construct, id: str, **kwargs):
        super().__init__(scope, id, **kwargs)

        stack = CDKTest2(self, "Stage")
