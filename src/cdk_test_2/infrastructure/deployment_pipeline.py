import aws_cdk.aws_codebuild as codebuild
import aws_cdk.aws_codepipeline as codepipeline
import aws_cdk.aws_codepipeline_actions as cpactions
import aws_cdk.aws_sns as sns
import aws_cdk.aws_ssm as ssm
import aws_cdk.core as core
import aws_cdk.pipelines as pipelines
import cdk_test_2.infrastructure.config as cfg
from aws_cdk.aws_codebuild import BuildEnvironmentVariableType
from aws_cdk.aws_iam import Effect, PolicyStatement
from cdk_test_2.infrastructure.deployment_stage import CDKTest2Stage


class CDKTest2Pipeline(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs):
        super().__init__(scope, id, **kwargs)

        source_artifact = codepipeline.Artifact()
        cloud_assembly_artifact = codepipeline.Artifact()
        approval_topic = sns.Topic.from_topic_arn(
            self,
            "PipelineApprovalTopic",
            topic_arn=ssm.StringParameter.value_for_string_parameter(
                self, parameter_name="/AXYS/DevOps/Utilities/PipelineApprovalTopicArn"
            ),
        )
        deployment_pipeline = codepipeline.Pipeline(
            self,
            "CDKTest2-Pipeline",
            pipeline_name=id,
            restart_execution_on_update=True,
            cross_account_keys=cfg.IS_PRODUCTION,
            stages=[
                codepipeline.StageOptions(
                    stage_name="Bitbucket-Source-Action",
                    actions=[
                        cpactions.BitBucketSourceAction(
                            action_name="Bitbucket-Commit-Webhook",
                            output=source_artifact,
                            connection_arn=cfg.bitbucket_connection_arn,
                            owner=cfg.repo_owner,
                            repo=cfg.repo_name,
                            branch=cfg.BRANCH,
                            code_build_clone_output=True,
                            run_order=1,
                        ),
                    ],
                ),
                codepipeline.StageOptions(
                    stage_name="Bitbucket-Test-Pass-Approval",
                    actions=[
                        cpactions.ManualApprovalAction(
                            action_name="WaitForTests",
                            run_order=2,
                            notification_topic=approval_topic,
                        ),
                    ],
                ),
            ],
        )

        pipeline = pipelines.CdkPipeline(
            self,
            id,
            code_pipeline=deployment_pipeline,
            cloud_assembly_artifact=cloud_assembly_artifact,
            synth_action=pipelines.SimpleSynthAction(
                source_artifact=source_artifact,
                cloud_assembly_artifact=cloud_assembly_artifact,
                install_commands=[
                    # Keeping up to date with pyenv on AWS is hard, and we are guaranteed in the 3.8.X range, which is good enough
                    "rm .python-version",
                    "curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -",
                    ". $HOME/.poetry/env",
                    "export POETRY_HTTP_BASIC_AXYS_PASSWORD=`aws codeartifact get-authorization-token --domain axys-technologies --domain-owner 053434365272 --query authorizationToken --output text`",
                    "export POETRY_HTTP_BASIC_AXYS_USERNAME=aws",
                    "pip config set global.extra-index-url https://$POETRY_HTTP_BASIC_AXYS_USERNAME:$POETRY_HTTP_BASIC_AXYS_PASSWORD@axys-technologies-053434365272.d.codeartifact.us-west-2.amazonaws.com/pypi/axys-pip/simple/",
                    'pip config set global.trusted-host "pypi.org axys-technologies-053434365272.d.codeartifact.us-west-2.amazonaws.com"',
                    "docker login -u $DOCKER_USER -p $DOCKER_PASS",
                ],
                build_commands=[
                    "npm install -g aws-cdk",
                    "python -m venv .venv",  # TODO: Remove this once hdlc package is pushed to PyPi.
                    ". .venv/bin/activate",  # TODO: Remove this once hdlc package is pushed to PyPi.
                    "pip install --upgrade pip",  # TODO: Remove this once hdlc package is pushed to PyPi.
                    "pip install setuptools-scm",  # TODO: Remove this once hdlc package is pushed to PyPi.
                    "poetry install",
                ],
                synth_command="cdk synth",
                environment=codebuild.BuildEnvironment(
                    privileged=True,
                    environment_variables={
                        "SOURCE_BRANCH_NAME": codebuild.BuildEnvironmentVariable(value=cfg.BRANCH),
                        "DOCKER_USER": codebuild.BuildEnvironmentVariable(
                            value="/AXYS/DevOps/Utilities/DockerHub/username", type=BuildEnvironmentVariableType.PARAMETER_STORE
                        ),
                        "DOCKER_PASS": codebuild.BuildEnvironmentVariable(
                            value="/AXYS/DevOps/Utilities/DockerHub/password", type=BuildEnvironmentVariableType.PARAMETER_STORE
                        ),
                    },
                    # The build environment being used is from the previous iteration of the pipeline:
                    #   IE: The first run on codepipeline uses the cfg.BRANCH set from the developers machine.
                    #       Run 2 uses the variable set from run 1.
                ),
                role_policy_statements=[
                    PolicyStatement(
                        actions=["ssm:GetParameter", "ssm:GetParameters", "kms:Decrypt"],
                        effect=Effect.ALLOW,
                        resources=[
                            "arn:aws:ssm:us-west-2:053434365272:parameter/AXYS/DevOps/*"
                            "arn:aws:kms:us-west-2:053434365272:key/964892fc-60b5-4333-9b85-9105853994fb",
                        ],
                    ),
                    # These two statements provide codeartifact access to the pipeline. Subcontainers can mount the pypi.conf to inherit them.
                    PolicyStatement(
                        actions=[
                            "codeartifact:GetAuthorizationToken",
                            "codeartifact:GetRepositoryEndpoint",
                            "codeartifact:ReadFromRepository",
                            "codeartifact:*",
                        ],
                        effect=Effect.ALLOW,
                        resources=["*"],
                    ),
                    PolicyStatement(
                        actions=["sts:GetServiceBearerToken"],
                        effect=Effect.ALLOW,
                        resources=["*"],
                        conditions={"StringEquals": {"sts:AWSServiceName": "codeartifact.amazonaws.com"}},
                    ),
                ],
            ),
        )

        pipeline.add_application_stage(CDKTest2Stage(self, "CDKTest2", env=cfg.env))
