import hashlib
import os
import re
from os.path import dirname

import git

######################################################################
# DO NOT EDIT ANYTHING IN THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING
######################################################################
application_name = "CDKTest2"

repo_owner = "axysjparker"
repo_name = "cdk-test-2"
default_region = "us-west-2"

bitbucket_connection_arn = (
    "arn:aws:codestar-connections:us-west-2:053434365272:connection/cbc1a70e-b43f-444a-94ab-2fd3ff871cb4"  # AXYSJparker
    # "arn:aws:codestar-connections:us-west-2:053434365272:connection/86e4b4af-05cd-49ba-bec9-b4cc948cbbe9"  # AXYSBitBucket Connection
)
##################
# Branch Parsing
##################


def str_to_hash(s):
    return hashlib.md5(s.encode()).hexdigest()[:4].upper()


try:
    # Read when deploying from a dev machine.
    BRANCH = git.Repo(os.getcwd()).active_branch.name
except:
    # Carried forward from the original value read on the dev machine.
    BRANCH = os.environ.get("SOURCE_BRANCH_NAME", None)
assert BRANCH is not None

# Treat master and main as the same
if BRANCH == "master":
    BRANCH = "main"

IS_PRODUCTION = BRANCH == "prod"
env = {"account": "978983817882" if IS_PRODUCTION else "053434365272", "region": default_region}


# Extracts relevant info from branch name
def parse_branch(b):
    if b == "prod":
        return "PROD"
    if b == "main":
        return "MAIN"
    if b == "develop":
        return "DEV"

    branch_format = re.compile(
        r"^(?P<author>[\w-]+)/(?P<type>[\w\/-]*?)(?:(?:AC-(?P<ticket>\d{1,4})-?(?P<extra>[\w]+)?/)?(?P<desc>[\w\d-]*))$"
    )
    if (matches := branch_format.search(b)) :
        md = matches.groupdict()
        BRANCH_AUTHOR = md.get("author", "AXYS")
        BRANCH_TYPE = md.get("type", "misc/")[-1:]  # Captures as feature/ always
        BRANCH_TICKET = md.get("ticket") or ""
        BRANCH_EXTRA = md.get("extra") or ""
        BRANCH_DESC = md.get("desc") or ""
        if not BRANCH_TICKET:
            BRANCH_TICKET = str_to_hash(f"{BRANCH_AUTHOR}{BRANCH_TYPE}")
        return f"{BRANCH_TICKET}{'-'+BRANCH_EXTRA[:4].upper() if BRANCH_EXTRA else ''}"

    raise Exception(f"Invalid branch format: {BRANCH}")


pipeline_tag = parse_branch(BRANCH)
pipeline_name = f"{application_name}-{pipeline_tag}"
