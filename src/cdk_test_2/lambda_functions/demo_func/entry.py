import logging

import pytest
import sample_package
from axys_aws_libs import LambdaBase, LambdaHandler


@LambdaHandler
class SampleFunction(LambdaBase):
    def __init__(self) -> None:
        super().__init__()

    def handler(event, context, log: logging.Logger):
        log.info("SampleLogging - Info")
        log.warn("SampleLogging - Warn")
        log.error("SampleLogging - Error")
        log.debug("SampleLogging - Debug")

        log.info(f"This function installed pytest for no reason: {pytest.__version__}")
        log.info(f"It also imported a local lambda layer: {sample_package.hello_world()}")

        return "Hello From Lambda, Jason Parker. This is CDK Test 2"
