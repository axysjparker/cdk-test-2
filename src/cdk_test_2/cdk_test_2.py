from os import path

import aws_cdk.core as core
from axys_cdk_constructs import PythonCode, PythonFunctionBase, PythonLayerBase


class CDKTest2(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)
        this_dir = path.dirname(__file__)

        # Names for constructs should be concise and minimal. Context is added to them automatically!
        layer = PythonLayerBase(self, "Layer", layer_path=path.join(this_dir, "lambda_layers/demo_layer"))
        func = PythonFunctionBase(
            self,
            "Function",
            # PythonCode bundles the application with requirements.txt
            code=PythonCode(
                asset_path=path.join(this_dir, "lambda_functions/demo_func"),
            ),
            # AXYS_AWS_LIBS is included via the common lambda layer by default.
            # Other named layers can be attached using their name:
            # >>> layer_names = ["some_layer", "hdlc"],
            layers=[layer],
        )
