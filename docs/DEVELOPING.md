
# Getting Started

## Tools Used

* pyenv
* poetry
* nvm
* pipx (not required, but recommended)

## Setup Process
Eventually this could be automated, but for now it's being done by hand. First, get the main tools out of the way:

```bash
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

# Relaunching the shell so these are loaded
exec $SHELL

# NodeJS
nvm install --lts

# Python 3.8.6
pyenv install 3.8.6
pyenv global 3.8.6 # Setting default python version explicitly to 3.8.6

# Poetry Package Management
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python

# AWS CLI V2
cd ~
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

# Pipx (Optional. Enables standalone python applications to run in their own isolated environment without impacting any other system. Very useful)
python -m pip install --user pipx
python -m pipx ensurepath

# Install the AWS SSO Utility
pipx install aws-sso-util
pipx install aws-export-credentials


```

To manage AWS login credentials easily through SSO, add the following to your `~/.bashrc` or `~/.zshrc`:
```bash
export AWS_DEFAULT_SSO_START_URL=https://axys.awsapps.com/start
export AWS_DEFAULT_SSO_REGION=us-west-2
export AWS_CONFIGURE_SSO_DEFAULT_SSO_PROFILE=developer
export CODEARTIFACT_USER=aws
axys-pypi-login() {
  unset CODEARTIFACT_AUTH_TOKEN
  pip config unset global.index-url
  pip config unset global.extra-index-url
  export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain axys-technologies --domain-owner 053434365272 --query authorizationToken --output text`
  pip config set global.extra-index-url https://$CODEARTIFACT_USER:$CODEARTIFACT_AUTH_TOKEN@axys-technologies-053434365272.d.codeartifact.us-west-2.amazonaws.com/pypi/axys-pip/simple/
  pip config set global.trusted-host "pypi.org axys-technologies-053434365272.d.codeartifact.us-west-2.amazonaws.com"
  unset POETRY_HTTP_BASIC_AXYS_PASSWORD
  export POETRY_HTTP_BASIC_AXYS_USERNAME=$CODEARTIFACT_USER
  export POETRY_HTTP_BASIC_AXYS_PASSWORD=$CODEARTIFACT_AUTH_TOKEN
  echo "CODEARTIFACT_AUTH_TOKEN refreshed; POETRY_HTTP_BASIC variables set for this shell"
  echo "NOTICE: pip uses default index, to configure AXYS PyPi as default run axys-pypi-global"
}

axys-pypi-global() {
  pip config set global.index-url https://$CODEARTIFACT_USER:$CODEARTIFACT_AUTH_TOKEN@axys-technologies-053434365272.d.codeartifact.us-west-2.amazonaws.com/pypi/axys-pip/simple/
  pip config set global.extra-index-url http://pypi.org/simple/
  echo "NOTICE: Setting pip's default index to AXYS-Pip"
}

axys-pypi-logout() {
  pip config unset global.index-url
  pip config unset global.extra-index-url
  unset CODEARTIFACT_AUTH_TOKEN
  unset POETRY_HTTP_BASIC_AXYS_PASSWORD
}

axys-sso(){
  unset AWS_PROFILE
  export AWS_PROFILE=${1:-$AWS_CONFIGURE_SSO_DEFAULT_SSO_PROFILE}
  aws-sso-util login
  aws sts get-caller-identity &> /dev/null || (unset AWS_PROFILE && aws-sso-util configure profile ${1:-$AWS_CONFIGURE_SSO_DEFAULT_SSO_PROFILE})
  eval $(aws-export-credentials --env-export)
  axys-pypi-login
  echo "Set AWS SSO Profile to ${1:-$AWS_CONFIGURE_SSO_DEFAULT_SSO_PROFILE}. Profile details available in ~/.aws/config"
}


```
This adds a command line function that will automatically log you into a profile, defaulting to one named developer. For initial setup, just run `sso` and follow the prompts to select the AXYS Sandbox account and AXYS-Developer role. From then on to login to AWS just execute `sso`. Additional roles can be configured by passing a role name to the command: `sso admin` will create a new SSO profile named admin and walk you through associating it with a AWS Account and IAM role.

Next, configure the actual private PyPI Server inside of poetry:
```bash
poetry config repositories.axys $(aws codeartifact get-repository-endpoint --domain axys-technologies --domain-owner 053434365272 --repository axys-pip --query repositoryEndpoint --output text)/simple/
```



# Guidelines and Best Practices
This needs to be updated.

* SemVer, and only SemVer.
* Poetry only for dependencies
* Clean commit messages. Commits must follow [ConventionalCommits](https://www.conventionalcommits.org/en/v1.0.0/). Jira tags will be auto added as a footer to the commit if applicable.
