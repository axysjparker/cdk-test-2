import logging
import mimetypes
import os
import sys
from io import StringIO

import boto3
import git

log = logging.getLogger(__name__)

s3 = boto3.resource("s3")

report_bucket = s3.Bucket("axys-build-history")
repo = git.Repo(search_parent_directories=True)
real_commit_hash = repo.head.object.hexsha
commit_hash = os.environ.get("BITBUCKET_COMMIT", "commit")

# assert commit_hash == real_commit_hash

repo_slug = os.environ.get("BITBUCKET_REPO_SLUG", "repo")
branch = os.environ.get("BITBUCKET_BRANCH", "branch")
pr_id = os.environ.get("BITBUCKET_PR_ID", -1)

# I am ashamed of this, but it works...
ASSETS_COV = {
    "jquery.min.js": '    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>\n',
    "jquery.ba-throttle-debounce.min.js": '    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>\n',
    "jquery.hotkeys.js": '    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.hotkeys/0.1.0/jquery.hotkeys.min.js"></script>\n',
    # "jquery.jsonscreen.js": '    <script type="text/javascript" src="https://builds.axysdev.com/reports/assets/jquery.jsonscreen.js"></script>',
    "jquery.tablesorter.min.js": '    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.min.js"></script>\n',
    "keybd_closed.png": '        <img id="keyboard_icon" src="https://builds.axysdev.com/reports/assets/keybd_closed.png" alt="Show keyboard shortcuts" />\n',
    "keybd_open.png": '    <img id="panel_icon" src="https://builds.axysdev.com/reports/assets/keybd_open.png" alt="Hide keyboard shortcuts" />\n',
    "style.css": '    <link rel="stylesheet" href="https://builds.axysdev.com/reports/assets/cov.css" type="text/css">\n',
    "coverage_html.js": '    <script type="https://builds.axysdev.com/reports/assets/coverage_html.js"></script>\n',
}
ASSETS_TEST = {
    "assets/style.css": '    <link href="https://builds.axysdev.com/reports/assets/style.css" rel="stylesheet" type="text/css"/></head>\n'
}


def main():
    key_prefix = f"{repo_slug}/{branch}/{commit_hash}"
    upload_logs("htmlcov", key_prefix, ASSETS_COV)
    upload_logs("test-reports", key_prefix, ASSETS_TEST)

    if os.environ.get("TEST_RESULT") == "FAIL":
        sys.exit(1)


def get_commit_files():
    last_commit_files = repo.commit().stats.files.keys()
    log.debug(last_commit_files)


def rewrite(file, asset_map):
    result = StringIO()
    am = asset_map.copy()
    with open(file, "r") as f:
        for line in f:
            match = [key for key in am.keys() if key in line]
            if len(match) > 0:
                key = match[0]
                line = am[key]
                del am[key]
            result.write(line)

    result.seek(0)
    return result


def upload_logs(directory, prefix, asset_map):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file[-4:] in ["html", ".xml"]:
                file_name = f"{root}/{file}"
                content_type = mimetypes.guess_type(file_name)[0]
                object_name = f"{prefix}/{file_name}"
                report_bucket.put_object(
                    Key=object_name,
                    Body=rewrite(file_name, asset_map).read(),
                    ContentType=content_type,
                )


# def up():
#     for root, dirs, files in os.walk("assets"):
#         for file in files:
#             fn = f"{root}/{file}"
#             content_type = mimetypes.guess_type(fn)[0]
#             log.debug(f"assets/{file}, {content_type}")
#             with open(fn, "rb") as f:
#                 report_bucket.put_object(Key=f"assets/{file}", Body=f, ContentType=content_type)


if __name__ == "__main__":
    main()
