import json
import os
import sys
import urllib.parse as urlparse
from datetime import datetime

import boto3
import git

# imported from src to be non-install runnable
from src.cdk_test_1.infrastructure.config import BRANCH, pipeline_name

# Extracts relevant info from branch name

try:
    BITBUCKET_COMMIT = os.environ.get("BITBUCKET_COMMIT")
    BITBUCKET_BUILD_NUMBER = os.environ.get("BITBUCKET_BUILD_NUMBER", "na")
    AC_APPROVAL_TABLE = os.environ.get("AC_APPROVAL_TABLE", "PipelineApproval-ApprovalTable499CB07A-U9EOART1APX0")

    COMMIT_MESSAGE = git.Repo(os.getcwd()).commit(BITBUCKET_COMMIT).message
    COMMIT_MESSAGE_SKIP_DEPLOY = "[skip deploy]" in COMMIT_MESSAGE

    PIPELINE_NAME = pipeline_name
    BRANCH_NAME = BRANCH

    APPROVAL = os.environ.get("BITBUCKET_EXIT_CODE", "1") == "0"
    PR_ID = os.environ.get("BITBUCKET_PR_ID", -1)
except Exception as e:
    print(e)
    print("Running in a non-bitbucket-pipeline environment, exiting.")
    sys.exit(1)

codepipeline = boto3.client("codepipeline")
approval_table = boto3.resource("dynamodb").Table(AC_APPROVAL_TABLE)

SOURCE_STAGE = "Bitbucket-Source-Action"
SOURCE_ACTION = "Bitbucket-Commit-Webhook"
APPROV_STAGE = "Bitbucket-Test-Pass-Approval"
APPROV_ACTION = "WaitForTests"


def get_execution_id():
    try:
        curr_state = codepipeline.get_pipeline_state(name=PIPELINE_NAME)
    except:
        print(f"No pipeline named {PIPELINE_NAME} found")
        return None
    for stage in curr_state["stageStates"]:
        if stage["stageName"] == SOURCE_STAGE:
            for action in stage["actionStates"]:
                if action["actionName"] == SOURCE_ACTION and action["currentRevision"]["revisionId"] == BITBUCKET_COMMIT:
                    return stage["latestExecution"]["pipelineExecutionId"]
    return None


def get_action_approval_token(ex_id):
    curr_state = codepipeline.get_pipeline_state(name=PIPELINE_NAME)
    for stage in curr_state["stageStates"]:
        if stage["stageName"] == APPROV_STAGE and stage["latestExecution"]["pipelineExecutionId"] == ex_id:
            for action in stage["actionStates"]:
                if action["actionName"] == APPROV_ACTION and action["latestExecution"]["status"] == "InProgress":
                    return action["latestExecution"]["token"]
    return None


def set_tracked_branch(name):
    tracked_branches = approval_table.get_item(Key={"id": "meta", "sk": "meta"}, ProjectionExpression="tracked_branches")["Item"][
        "tracked_branches"
    ]

    if name not in tracked_branches:
        tracked_branches += name
        r = approval_table.update_item(
            Key={"id": "meta", "sk": "meta"},
            UpdateExpression="SET tracked_branches = list_append(tracked_branches, :i)",
            ExpressionAttributeValues={
                ":i": [name],
            },
            ReturnValues="NONE",
        )


def grant_approval(commit_hash):
    # Grant approval for the commit if the pipeline hasn't reached that stage yet
    approval_table.put_item(
        Item={
            "id": commit_hash,
            "sk": datetime.now().astimezone().isoformat(),
            "pipeline": PIPELINE_NAME,
            "approval_granted": APPROVAL,
            "branch_name": BRANCH_NAME,
            "pullrequest_id": PR_ID,
            "build_number": BITBUCKET_BUILD_NUMBER,
        }
    )

    set_tracked_branch(BRANCH_NAME)

    print(f"Added approval entry ({APPROVAL}) for {commit_hash} on {PIPELINE_NAME}")

    curr_ex_id = get_execution_id()
    print(curr_ex_id, "execution")
    if curr_ex_id is not None:
        if COMMIT_MESSAGE_SKIP_DEPLOY:
            result = {
                "summary": f"Bitbucket pipeline {BITBUCKET_BUILD_NUMBER} triggered deployment skip",
                "status": "Rejected",
            }
            resp = codepipeline.stop_pipeline_execution(
                pipelineName=PIPELINE_NAME,
                pipelineExecutionId=curr_ex_id,
                abandon=True,
                reason="Bitbucket Pipeline triggered deployment skip",
            )
        elif APPROVAL:
            result = {
                "summary": f"Bitbucket Tests Passed (pipeline {BITBUCKET_BUILD_NUMBER}), approval granted automatically.",
                "status": "Approved",
            }
        else:
            result = {
                "summary": f"Bitbucket Tests Failed (pipeline {BITBUCKET_BUILD_NUMBER}), approval rejected.",
                "status": "Rejected",
            }
            resp = codepipeline.stop_pipeline_execution(
                pipelineName=PIPELINE_NAME,
                pipelineExecutionId=curr_ex_id,
                abandon=True,
                reason="Bitbucket Pipeline failed, abandoning CodePipeline Execution automatically",
            )
            print(f"approval not given, terminating execution id {curr_ex_id}")

        if (token := get_action_approval_token(curr_ex_id)) is not None:
            resp = codepipeline.put_approval_result(
                pipelineName=PIPELINE_NAME,
                stageName=APPROV_STAGE,
                actionName=APPROV_ACTION,
                result=result,
                token=token,
            )

            print(f"Gave approval to waiting action on {PIPELINE_NAME} with execution_id {curr_ex_id}")
    else:
        print("failed on approval")


def main():
    grant_approval(BITBUCKET_COMMIT)
    if not APPROVAL:
        print("Exiting with Status Code 1: Tests failed.")
        sys.exit(1)


if __name__ == "__main__":
    main()
