# CDK Test 2



# Getting Started

This project is configured with an AWS CDK Pipeline, and integrates as a base into the AXYS Data Pipeline.

To configure the development environment and its prerequisits, check out [`docs\DEVELOPING.md`](src/DEVELOPING.md).


# Setup

Inside the root of the repository run:
0. Run `axys-sso` to login to the AXYS PyPi server and CLI.
1. `git init`
2. `git remote add origin git@bitbucket.org:axysjparker/cdk-test-2.git`
3. `pre-commit install`
4. `poetry install`
5. `source .venv/bin/activate`
6. `pytest tests`
7. `cdk synth`
8. Commit to the repository named cdk-test-2. Be sure to re-add and commit if a pre-commit hook modifies a file!
9. `cdk deploy`

# Branch Naming Conventions

Branches must be either one of **`prod`, `main`, `develop`** OR match the following regex:

```python
regex = r"^(?:(?P<author>[\w-]+)/)(?P<type>[\w\-\/]+?)(?:AC-(?P<ticket>\d{1,4})(?:-(?P<extra>[\w\d]+))?)?(?:/(?P<desc>[\w]*))$"
```

Which translates to branches named off of the following:

```
AUTHOR/TYPE/AC-TICKET[-EXTRA][/DESCRIPTION]
```
* `AUTHOR`: Author of the branch
* `TYPE`: Type is a classifier of the branch, and will expand up to the ticket number. All of the following are valid:
    * `feature`
    * `bugfix`
    * `hotfix`
    * `chore`
    * `refactor`
    * `experimental`
    * `
* `AC-TICKET`: Ticket's *must* be in the form of "AC-###".
