#!/usr/bin/env python3

from aws_cdk import core
from cdk_test_2.cdk_test_2 import CDKTest2
from cdk_test_2.infrastructure import config as cfg
from cdk_test_2.infrastructure.deployment_pipeline import CDKTest2Pipeline

app = core.App()

CDKTest2Pipeline(app, f"{cfg.pipeline_name}-CI", env=cfg.env)

# A local synth can be triggered to resolve context using the base stack deployed by CDK Pipeliens, but should not be deployed standalone:
# CDKTest2(app, "CDKTest2-DONOTDEPLOY", env=cfg.env)

app.synth()
